# Taspen ZK POC Project

## 1. Technologies
### Framework:
* Web & UI Layer: [ZK Framework v8.0.5-EE-Eval](https://www.zkoss.org/)
* Business Layer: [Spring Framework v4.2.6](https://spring.io/)
* Persistence Layer: [Hibernate Framework v4.3.11](http://hibernate.org/)
* Bean Validator: [Hibernate Validator v5.2.4](http://hibernate.org/validator/)
* Security Layer: [Spring Security](https://projects.spring.io/spring-security/)

### Dependency Management & Build System:
* [Apache Maven 3](https://maven.apache.org/)

### IDE:
* [NetBeans IDE v8.2](https://netbeans.org/)

### JVM:
* [JDK 8u121](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

### Database:
* [SQLite3](https://www.sqlite.org/)

## 2. Install & Run the Application
1. Load the project with NetBeans IDE or whatever IDE you prefered.
2. Run with Java any prefered servlet container application server:
	*[Apache Tomcat](http://tomcat.apache.org/)
	*[Eclipse Jetty](http://www.eclipse.org/jetty/)
3. Coming soon........

