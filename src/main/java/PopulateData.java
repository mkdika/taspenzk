
import com.mkdika.taspenzk.config.SpringCfg;
import com.mkdika.taspenzk.model.PA9030;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.mkdika.taspenzk.repository.DosirTransRepo;
import com.mkdika.taspenzk.repository.PA9010Repo;
import com.mkdika.taspenzk.repository.PA9030Repo;
import com.mkdika.taspenzk.repository.PA9040Repo;
import java.util.Date;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
public class PopulateData {

    private static ApplicationContext ctx;
    private static DosirTransRepo dosirRepo;
    private static PA9010Repo pa9010Repo;
    private static PA9030Repo pa9030Repo;
    private static PA9040Repo pa9040Repo;

    public static void main(String[] args) {

        // Init the Spring Application Context
        initAppContext();

        populateInitData();
    }

    public static void populateInitData() {

    }

    private static void initAppContext() {
        ctx = new AnnotationConfigApplicationContext(SpringCfg.class);
        dosirRepo = ctx.getBean(DosirTransRepo.class);
        pa9010Repo = ctx.getBean(PA9010Repo.class);
        pa9030Repo = ctx.getBean(PA9030Repo.class);
        pa9040Repo = ctx.getBean(PA9040Repo.class);
    }

    private static void testInsertAndQuery() {
        PA9030 p = new PA9030();
        p.setNoDosir("1234");
        p.setPersonelNumber("ABC000");
        p.setStartDate(new Date());
        p.setEndDate(new Date());
        p.setScanDate(new Date());
        pa9030Repo.save(p);
        System.out.println("Save!!!");

        PA9030 p1 = pa9030Repo.findAll().iterator().next();
        System.out.println("ID: " + p1.getId());
        System.out.println("NO DOSIR: " + p1.getNoDosir());
    }

}
