package com.mkdika.taspenzk.repository;

import com.mkdika.taspenzk.model.PA9040;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
public interface PA9040Repo extends CrudRepository<PA9040, String>{
    
}
