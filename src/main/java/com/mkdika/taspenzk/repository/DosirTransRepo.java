package com.mkdika.taspenzk.repository;

import com.mkdika.taspenzk.model.DosirTrans;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
public interface DosirTransRepo extends CrudRepository<DosirTrans, String>{
    
}
