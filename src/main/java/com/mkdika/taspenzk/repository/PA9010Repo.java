package com.mkdika.taspenzk.repository;

import com.mkdika.taspenzk.model.PA9010;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
public interface PA9010Repo extends CrudRepository<PA9010, String>{
    
}
