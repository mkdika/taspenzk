package com.mkdika.taspenzk.repository;

import com.mkdika.taspenzk.model.PA9030;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
public interface PA9030Repo extends CrudRepository<PA9030, String>{
    
}
