package com.mkdika.taspenzk.service;

import com.mkdika.taspenzk.repository.DosirTransRepo;
import com.mkdika.taspenzk.repository.PA9010Repo;
import com.mkdika.taspenzk.repository.PA9030Repo;
import com.mkdika.taspenzk.repository.PA9040Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
@Service("appService")
@Transactional
public class AppService {

    @Autowired
    private DosirTransRepo dosirRepo;
    
    @Autowired
    private PA9010Repo pa9010Repo;
    
    @Autowired
    private PA9030Repo pa9030Repo;
    
    @Autowired
    private PA9040Repo pa9040Repo;

    public DosirTransRepo getDosirRepo() {
        return dosirRepo;
    }

    public PA9010Repo getPa9010Repo() {
        return pa9010Repo;
    }

    public PA9030Repo getPa9030Repo() {
        return pa9030Repo;
    }

    public PA9040Repo getPa9040Repo() {
        return pa9040Repo;
    }        
}
