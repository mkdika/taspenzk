package com.mkdika.taspenzk.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
@Entity
@Table(name = "pa9010")
public class PA9010 implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", length = 32, nullable = false)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    
    @Column(name = "personel_number", length = 100, nullable = false)
    private String personelNumber;
    
    @Column(name = "nomor_notas", length = 100, nullable = false)
    private String nomorNotas;
    
    @Column(name = "nip", length = 100, nullable = false)
    private String nip;
    
    @Column(name = "nomor_kpe", length = 100, nullable = false)
    private String nomorKpe;
    
    @Column(name = "nik", length = 100, nullable = false)
    private String nik;
    
    @Column(name = "kode_cabang", length = 100, nullable = false)
    private String kodeCabang;
    
    @Column(name = "bulan_tahun_gaji", length = 100)
    private String bulanTahunGaji;

    public PA9010() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPersonelNumber() {
        return personelNumber;
    }

    public void setPersonelNumber(String personelNumber) {
        this.personelNumber = personelNumber;
    }

    public String getNomorNotas() {
        return nomorNotas;
    }

    public void setNomorNotas(String nomorNotas) {
        this.nomorNotas = nomorNotas;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNomorKpe() {
        return nomorKpe;
    }

    public void setNomorKpe(String nomorKpe) {
        this.nomorKpe = nomorKpe;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getBulanTahunGaji() {
        return bulanTahunGaji;
    }

    public void setBulanTahunGaji(String bulanTahunGaji) {
        this.bulanTahunGaji = bulanTahunGaji;
    }        
}
