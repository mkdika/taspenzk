
package com.mkdika.taspenzk.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
@Entity
@Table(name = "pa9030")
public class PA9030 implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id", length = 32, nullable = false)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")    
    private String id;
    
    @Column(name = "personel_number", length = 100, nullable = false)
    private String personelNumber;
    
    @Column(name = "start_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;
    
    @Column(name = "end_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;
    
    @Column(name = "no_dosir", length = 100, nullable = false)
    private String noDosir;
    
    @Column(name = "scan_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date scanDate;
    
    public PA9030() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPersonelNumber() {
        return personelNumber;
    }

    public void setPersonelNumber(String personelNumber) {
        this.personelNumber = personelNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNoDosir() {
        return noDosir;
    }

    public void setNoDosir(String noDosir) {
        this.noDosir = noDosir;
    }

    public Date getScanDate() {
        return scanDate;
    }

    public void setScanDate(Date scanDate) {
        this.scanDate = scanDate;
    }            
}
