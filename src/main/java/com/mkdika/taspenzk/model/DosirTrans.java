package com.mkdika.taspenzk.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
@Entity
@Table(name = "dosir_trans")
public class DosirTrans implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", length = 32, nullable = false)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Column(name = "kode", length = 100, nullable = false)
    private String kode;

    @Column(name = "kode_cabang", length = 100, nullable = false)
    private String kodeCabang;

    @Column(name = "nomor_dosir", length = 100, nullable = false)
    private String nomorDosir;

    @Column(name = "kode_hub_keluarga", length = 100)
    private String kodeHubKeluarga;

    @Column(name = "notas", length = 100, nullable = false)
    private String notas;

    @Column(name = "tgl_pinjam")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglPinjam;

    @Column(name = "tgl_kembali")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglKembali;

    @Column(name = "kode_uraian_dosir", length = 100, nullable = false)
    private String kodeUraianDosir;

    @Column(name = "pemakai", length = 100, nullable = false)
    private String pemakai;

    @Column(name = "keterangan", length = 255)
    private String keterangan;

    @Column(name = "petugas_dosir", length = 100, nullable = false)
    private String petugasDosir;

    @Column(name = "atasan_pemakai")
    private String atasanPemakai;

    @Column(name = "tipe", length = 100, nullable = false)
    private String tipe;

    public DosirTrans() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getNomorDosir() {
        return nomorDosir;
    }

    public void setNomorDosir(String nomorDosir) {
        this.nomorDosir = nomorDosir;
    }

    public String getKodeHubKeluarga() {
        return kodeHubKeluarga;
    }

    public void setKodeHubKeluarga(String kodeHubKeluarga) {
        this.kodeHubKeluarga = kodeHubKeluarga;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Date getTglPinjam() {
        return tglPinjam;
    }

    public void setTglPinjam(Date tglPinjam) {
        this.tglPinjam = tglPinjam;
    }

    public Date getTglKembali() {
        return tglKembali;
    }

    public void setTglKembali(Date tglKembali) {
        this.tglKembali = tglKembali;
    }

    public String getKodeUraianDosir() {
        return kodeUraianDosir;
    }

    public void setKodeUraianDosir(String kodeUraianDosir) {
        this.kodeUraianDosir = kodeUraianDosir;
    }

    public String getPemakai() {
        return pemakai;
    }

    public void setPemakai(String pemakai) {
        this.pemakai = pemakai;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getPetugasDosir() {
        return petugasDosir;
    }

    public void setPetugasDosir(String petugasDosir) {
        this.petugasDosir = petugasDosir;
    }

    public String getAtasanPemakai() {
        return atasanPemakai;
    }

    public void setAtasanPemakai(String atasanPemakai) {
        this.atasanPemakai = atasanPemakai;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }   
}
